# Full-Stack Assignment

Creating Login/SignUp pages for imaginary website.

# Astro

Astro is a virtual company managing planet data from space.Providing data to various organizations on the planet, Astro helps in making better lives.

![register](https://github.com/hiverkiya/Astro/blob/master/img/register.png)
![login](https://github.com/hiverkiya/Astro/blob/master/img/login.png)

### Usage

* Clone/Download the repository 

* Either of register.html or login.html can be opened to view website.

#### Minor issues/bugs

* Although code has been added for responsiveness , it might not work for all devices.

* Backend integration is not provided.

